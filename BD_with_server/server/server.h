#ifndef SEVER_H
#define SEVER_H

#include <QWidget>
#include <vector>
#include <QtSql>

class QTcpServer;
class QTcpSocket;
class QTextEdit;

class Server: public QWidget{
    Q_OBJECT

private:
    QTcpServer *_tcpServer;
    QTcpSocket *_clientSocket;
    quint16 _nextBlockSize;
    QTextEdit *_text;
    bool state = true;
    QList<QTcpSocket*> sockets;
    void sendToClient(QTcpSocket *socket, const QString &str);
    QByteArray sha256(const QByteArray& text);
    QByteArray encrypt(const QByteArray& data);
    QByteArray decrypt(const QByteArray& data);
    QSqlDatabase _db;
    QSqlQuery _query;
public:
    Server(quint16 port);
public slots:
    virtual void slotNewConnection();
    void slotReadClient();
    void ClientDisconnected();
};

#endif // SEVER_H
