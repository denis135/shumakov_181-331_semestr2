/****************************************************************************
** Meta object code from reading C++ file 'auth.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../client/auth.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'auth.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Auth_t {
    QByteArrayData data[12];
    char stringdata0[148];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Auth_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Auth_t qt_meta_stringdata_Auth = {
    {
QT_MOC_LITERAL(0, 0, 4), // "Auth"
QT_MOC_LITERAL(1, 5, 15), // "on_auth_clicked"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 9), // "change_ui"
QT_MOC_LITERAL(4, 32, 15), // "on_sign_clicked"
QT_MOC_LITERAL(5, 48, 15), // "on_send_clicked"
QT_MOC_LITERAL(6, 64, 16), // "on_send_clicked2"
QT_MOC_LITERAL(7, 81, 12), // "table_update"
QT_MOC_LITERAL(8, 94, 13), // "QTableWidget*"
QT_MOC_LITERAL(9, 108, 5), // "table"
QT_MOC_LITERAL(10, 114, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(11, 136, 11) // "change_ui_2"

    },
    "Auth\0on_auth_clicked\0\0change_ui\0"
    "on_sign_clicked\0on_send_clicked\0"
    "on_send_clicked2\0table_update\0"
    "QTableWidget*\0table\0on_pushButton_clicked\0"
    "change_ui_2"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Auth[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x08 /* Private */,
       3,    0,   55,    2, 0x08 /* Private */,
       4,    0,   56,    2, 0x08 /* Private */,
       5,    0,   57,    2, 0x08 /* Private */,
       6,    0,   58,    2, 0x08 /* Private */,
       7,    1,   59,    2, 0x08 /* Private */,
      10,    0,   62,    2, 0x08 /* Private */,
      11,    0,   63,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Auth::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Auth *_t = static_cast<Auth *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_auth_clicked(); break;
        case 1: _t->change_ui(); break;
        case 2: _t->on_sign_clicked(); break;
        case 3: _t->on_send_clicked(); break;
        case 4: _t->on_send_clicked2(); break;
        case 5: _t->table_update((*reinterpret_cast< QTableWidget*(*)>(_a[1]))); break;
        case 6: _t->on_pushButton_clicked(); break;
        case 7: _t->change_ui_2(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QTableWidget* >(); break;
            }
            break;
        }
    }
}

const QMetaObject Auth::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Auth.data,
      qt_meta_data_Auth,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Auth::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Auth::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Auth.stringdata0))
        return static_cast<void*>(const_cast< Auth*>(this));
    return QWidget::qt_metacast(_clname);
}

int Auth::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
