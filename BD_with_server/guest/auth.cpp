#include "auth.h"
#include "ui_auth.h"
#include "QComboBox"
#include <QMessageBox>

Auth::Auth(user *user) :
    ui(new Ui::Auth),
    _user(user)
{
    ui->setupUi(this);
    _user->_cur_win = this;
    this->setAttribute( Qt::WA_DeleteOnClose, false );
}

Auth::~Auth()
{
    delete ui;
}

void Auth::on_send_clicked()
{
    QString name = line->text();
    QString age = line_2->text();
    QString sport = line_1->text();
    _user->slotSendToServer("<<<message>>>::" + name + "::" + age + "::" + sport);
    line->setText("");
    line_2->setText("");
    line_1->setText("");
    QMessageBox::information(this, "Успешно", "Рабочий успешно добавлен");
}



void Auth::change_ui(){
    ui->password->close();
    ui->login->close();
    ui->label->close();
    ui->label_2->close();
    ui->sign->close();
    ui->auth->close();
    ui->pushButton->close();

    layout_1 = new QVBoxLayout();
    QLabel *label = new QLabel();
    _table = new QTableWidget();
    _table->setRowCount(1);
    label->setText("<H1>Workers</H1>");
    label->setStyleSheet(QString::fromUtf8("padding-left: 180px;color:pink"));
    _user->setStyleSheet(QString::fromUtf8("background-color:white;"));
    layout_1 ->addWidget(label);
    layout_1->addWidget(_table);
    setLayout(layout_1);
    resize(500,300);
    setWindowTitle("user");
    _user->slotSendToServer("<<<get_messages>>> ");
}

void Auth::change_ui_2(){
    ui->password->close();
    ui->login->close();
    ui->label->close();
    ui->label_2->close();
    ui->sign->close();
    ui->auth->close();
    ui->pushButton->close();

    layout_1 = new QVBoxLayout();
    QHBoxLayout *layout_2 = new QHBoxLayout();
    QHBoxLayout *layout_3 = new QHBoxLayout();
    QHBoxLayout *layout_4 = new QHBoxLayout();




    line = new QLineEdit();
    line_2 = new QLineEdit();
    line_1 = new QLineEdit();
    QLabel *label = new QLabel();
    QLabel *label_1 = new QLabel();
    QLabel *label_2 = new QLabel();
    QLabel *label_3 = new QLabel();
    label_1->setText("ФИО          ");
    label_1->setStyleSheet(QString::fromUtf8("color:blue;"));
    label_2->setText("Бригада      ");
    label_2->setStyleSheet(QString::fromUtf8("color:green;"));
    label_3->setText("Информация   ");
    label_3->setStyleSheet(QString::fromUtf8("color:purple;"));
    QPushButton *button_1 = new QPushButton();
    button_1->setStyleSheet(QString::fromUtf8("background-color:red;color:black;"));
    connect(button_1, SIGNAL(clicked()), this, SLOT(on_send_clicked()));
    button_1->setText("Добавить");
    label->setText("<H1>Добавление рабочего</H1>");
    label->setStyleSheet(QString::fromUtf8("color:pink;padding-left:65px;"));


    layout_2->addWidget(label_1);
    layout_2->addWidget(line);
    layout_3->addWidget(label_2);
    layout_3->addWidget(line_2);
    layout_4->addWidget(label_3);
    layout_4->addWidget(line_1);



    layout_1->addWidget(label);
    layout_1->addLayout(layout_2);

    layout_1->addLayout(layout_3);
    layout_1->addLayout(layout_4);

    layout_1->addWidget(button_1);
    setLayout(layout_1);

    resize(400,315);
    setWindowTitle("admin");
}

void Auth::on_auth_clicked()
{

    QString login = ui->login->text();
    QString password = ui->password->text();
    _user->_login = login;
    _user->slotSendToServer("<<<auth>>> " + login + " " + password);
}





void Auth::on_sign_clicked()
{
    QString login = ui->login->text();
    QString password = ui->password->text();

    if (password == "" || login == ""){
        QMessageBox::warning(this, "warning", "Введите логин и пароль!");
    } else {
        _user->_login = login;
        _user->slotSendToServer("<<<signup>>> " + login + " " + password);
    }

}

void Auth::table_update(QTableWidget *table){
    layout_1->removeWidget(_table);
    _table = table;
    layout_1->addWidget(_table);
}


void Auth::on_pushButton_clicked()
{
    QString login = ui->login->text();
    QString password = ui->password->text();
    _user->_login = login;
    _user->slotSendToServer("<<<auth1>>> " + login + " " + password);
}
