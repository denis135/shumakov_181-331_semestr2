#ifndef AUTH_H
#define AUTH_H

#include <QWidget>
#include <user.h>
#include <QVBoxLayout>
#include <QComboBox>

namespace Ui {
class Auth;
}

class Auth : public QWidget
{
    Q_OBJECT

public:
    explicit Auth(user *user = nullptr);
    ~Auth();

private slots:
    void on_auth_clicked();
    void change_ui();
    void on_sign_clicked();
    void on_send_clicked();
    void table_update(QTableWidget *table);
    void on_pushButton_clicked();
    void change_ui_2();
private:
    Ui::Auth *ui;
    user *_user;
    QLineEdit *line;
    QLineEdit *line_1;
    QLineEdit *line_2;
    QComboBox *choice;
    QTableWidget *_table;
    QVBoxLayout *layout_1;

};

#endif // AUTH_H
