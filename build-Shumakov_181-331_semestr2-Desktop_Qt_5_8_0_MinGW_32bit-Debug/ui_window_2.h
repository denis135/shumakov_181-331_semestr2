/********************************************************************************
** Form generated from reading UI file 'window_2.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOW_2_H
#define UI_WINDOW_2_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_window_2
{
public:
    QLabel *label;
    QPushButton *Reg;
    QLabel *label_2;
    QLineEdit *pole;
    QLabel *label_3;
    QLineEdit *pole2;

    void setupUi(QDialog *window_2)
    {
        if (window_2->objectName().isEmpty())
            window_2->setObjectName(QStringLiteral("window_2"));
        window_2->resize(553, 465);
        window_2->setStyleSheet(QLatin1String("background-color: rgb(255, 243, 105);\n"
"background-color: rgb(253, 234, 168);"));
        label = new QLabel(window_2);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(100, 40, 341, 41));
        Reg = new QPushButton(window_2);
        Reg->setObjectName(QStringLiteral("Reg"));
        Reg->setGeometry(QRect(170, 330, 201, 41));
        Reg->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        label_2 = new QLabel(window_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(200, 100, 141, 16));
        pole = new QLineEdit(window_2);
        pole->setObjectName(QStringLiteral("pole"));
        pole->setGeometry(QRect(160, 130, 221, 41));
        pole->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        label_3 = new QLabel(window_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(200, 190, 151, 21));
        pole2 = new QLineEdit(window_2);
        pole2->setObjectName(QStringLiteral("pole2"));
        pole2->setGeometry(QRect(162, 249, 221, 41));
        pole2->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        pole2->setEchoMode(QLineEdit::Password);

        retranslateUi(window_2);

        QMetaObject::connectSlotsByName(window_2);
    } // setupUi

    void retranslateUi(QDialog *window_2)
    {
        window_2->setWindowTitle(QApplication::translate("window_2", "Dialog", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        label->setToolTip(QApplication::translate("window_2", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600; font-style:italic; color:#00007f;\">\320\240\320\265\320\263\320\270\321\201\321\202\321\200\320\260\321\206\320\270\321\217</span></p></body></html>", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("window_2", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600; font-style:italic; color:#00007f;\">\320\240\320\265\320\263\320\270\321\201\321\202\321\200\320\260\321\206\320\270\321\217</span></p></body></html>", Q_NULLPTR));
        Reg->setText(QApplication::translate("window_2", "\320\227\320\260\321\200\320\265\320\263\320\270\321\201\321\202\321\200\320\270\321\200\320\276\320\262\320\260\321\202\321\214\321\201\321\217", Q_NULLPTR));
        label_2->setText(QApplication::translate("window_2", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-weight:600;\">\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\273\320\276\320\263\320\270\320\275</span></p></body></html>", Q_NULLPTR));
        label_3->setText(QApplication::translate("window_2", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-weight:600;\">\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\277\320\260\321\200\320\276\320\273\321\214</span></p></body></html>", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class window_2: public Ui_window_2 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOW_2_H
