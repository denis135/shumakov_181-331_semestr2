/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QPushButton *Registr;
    QPushButton *Autorizaton;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(504, 349);
        MainWindow->setCursor(QCursor(Qt::OpenHandCursor));
        MainWindow->setMouseTracking(true);
        MainWindow->setStyleSheet(QStringLiteral("background-color: rgb(204, 0, 3);"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(130, 0, 241, 81));
        label->setMargin(24);
        label->setIndent(1);
        Registr = new QPushButton(centralWidget);
        Registr->setObjectName(QStringLiteral("Registr"));
        Registr->setGeometry(QRect(50, 180, 151, 81));
        Registr->setStyleSheet(QLatin1String("background-color: black;\n"
"color:white;\n"
"border-radius: 5px;\n"
""));
        Autorizaton = new QPushButton(centralWidget);
        Autorizaton->setObjectName(QStringLiteral("Autorizaton"));
        Autorizaton->setGeometry(QRect(300, 180, 151, 81));
        Autorizaton->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"border-radius:5px;\n"
""));
        MainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
#ifndef QT_NO_SHORTCUT
#endif // QT_NO_SHORTCUT

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600; font-style:italic; color:#000000;\">\320\227\320\264\321\200\320\260\320\262\321\201</span><span style=\" font-size:18pt; font-weight:600; font-style:italic; color:#ffffff;\">\321\202\320\262\321\203\320\271\321\202\320\265</span></p></body></html>", Q_NULLPTR));
        Registr->setText(QApplication::translate("MainWindow", "\320\240\320\265\320\263\320\270\321\201\321\202\321\200\320\260\321\206\320\270\321\217 ", Q_NULLPTR));
        Autorizaton->setText(QApplication::translate("MainWindow", "\320\220\320\262\321\202\320\276\321\200\320\270\320\267\320\260\321\206\320\270\321\217", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
