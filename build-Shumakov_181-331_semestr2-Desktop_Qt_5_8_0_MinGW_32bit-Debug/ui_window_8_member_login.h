/********************************************************************************
** Form generated from reading UI file 'window_8_member_login.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOW_8_MEMBER_LOGIN_H
#define UI_WINDOW_8_MEMBER_LOGIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_window_8_member_login
{
public:
    QLabel *label;
    QLineEdit *old_login;
    QLineEdit *old_password;
    QLineEdit *new_login;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QPushButton *save_login;

    void setupUi(QDialog *window_8_member_login)
    {
        if (window_8_member_login->objectName().isEmpty())
            window_8_member_login->setObjectName(QStringLiteral("window_8_member_login"));
        window_8_member_login->resize(700, 460);
        window_8_member_login->setStyleSheet(QStringLiteral("background-color: rgb(255, 151, 153);"));
        label = new QLabel(window_8_member_login);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(150, 100, 47, 13));
        old_login = new QLineEdit(window_8_member_login);
        old_login->setObjectName(QStringLiteral("old_login"));
        old_login->setGeometry(QRect(360, 130, 171, 31));
        old_login->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        old_password = new QLineEdit(window_8_member_login);
        old_password->setObjectName(QStringLiteral("old_password"));
        old_password->setGeometry(QRect(360, 290, 171, 31));
        old_password->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        new_login = new QLineEdit(window_8_member_login);
        new_login->setObjectName(QStringLiteral("new_login"));
        new_login->setGeometry(QRect(360, 210, 171, 31));
        new_login->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        label_2 = new QLabel(window_8_member_login);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(250, 10, 171, 51));
        label_3 = new QLabel(window_8_member_login);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(180, 130, 171, 31));
        label_4 = new QLabel(window_8_member_login);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(190, 210, 161, 31));
        label_5 = new QLabel(window_8_member_login);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(230, 290, 121, 31));
        save_login = new QPushButton(window_8_member_login);
        save_login->setObjectName(QStringLiteral("save_login"));
        save_login->setGeometry(QRect(300, 420, 111, 23));
        save_login->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        retranslateUi(window_8_member_login);

        QMetaObject::connectSlotsByName(window_8_member_login);
    } // setupUi

    void retranslateUi(QDialog *window_8_member_login)
    {
        window_8_member_login->setWindowTitle(QApplication::translate("window_8_member_login", "Dialog", Q_NULLPTR));
        label->setText(QString());
        label_2->setText(QApplication::translate("window_8_member_login", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-style:italic;\">\320\241\320\274\320\265\320\275\320\260 \320\273\320\276\320\263\320\270\320\275\320\260</span></p></body></html>", Q_NULLPTR));
        label_3->setText(QApplication::translate("window_8_member_login", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-style:italic;\">\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \321\201\321\202\320\260\321\200\321\213\320\271 \320\273\320\276\320\263\320\270\320\275</span></p></body></html>", Q_NULLPTR));
        label_4->setText(QApplication::translate("window_8_member_login", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-style:italic;\">\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\275\320\276\320\262\321\213\320\271 \320\273\320\276\320\263\320\270\320\275</span></p></body></html>", Q_NULLPTR));
        label_5->setText(QApplication::translate("window_8_member_login", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-style:italic;\">\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\277\320\260\321\200\320\276\320\273\321\214</span></p></body></html>", Q_NULLPTR));
        save_login->setText(QApplication::translate("window_8_member_login", "\320\241\320\274\320\265\320\275\320\270\321\202\321\214 \320\273\320\276\320\263\320\270\320\275", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class window_8_member_login: public Ui_window_8_member_login {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOW_8_MEMBER_LOGIN_H
