/********************************************************************************
** Form generated from reading UI file 'window_5_admin.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOW_5_ADMIN_H
#define UI_WINDOW_5_ADMIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_window_5_admin
{
public:
    QTextEdit *products;
    QTextEdit *rabi;
    QTextEdit *brigads;
    QTextEdit *journal;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QPushButton *Save;
    QPushButton *Repeat;
    QPushButton *exit;

    void setupUi(QDialog *window_5_admin)
    {
        if (window_5_admin->objectName().isEmpty())
            window_5_admin->setObjectName(QStringLiteral("window_5_admin"));
        window_5_admin->resize(730, 400);
        window_5_admin->setStyleSheet(QStringLiteral("background-color: rgb(170, 255, 127);"));
        products = new QTextEdit(window_5_admin);
        products->setObjectName(QStringLiteral("products"));
        products->setGeometry(QRect(220, 0, 281, 91));
        products->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"border-radius: 5px;\n"
"border-color: rgb(0, 0, 0);"));
        rabi = new QTextEdit(window_5_admin);
        rabi->setObjectName(QStringLiteral("rabi"));
        rabi->setGeometry(QRect(220, 90, 281, 91));
        rabi->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"border-color: rgb(0, 0, 0);\n"
"border-radius: 5px;\n"
""));
        brigads = new QTextEdit(window_5_admin);
        brigads->setObjectName(QStringLiteral("brigads"));
        brigads->setGeometry(QRect(220, 180, 281, 91));
        brigads->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"border-color: rgb(0, 0, 0);\n"
"border-radius: 5px;\n"
""));
        journal = new QTextEdit(window_5_admin);
        journal->setObjectName(QStringLiteral("journal"));
        journal->setGeometry(QRect(220, 270, 281, 91));
        journal->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"border-color: rgb(0, 0, 0);\n"
"border-radius: 5px;\n"
""));
        label = new QLabel(window_5_admin);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 40, 201, 20));
        label_2 = new QLabel(window_5_admin);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(520, 40, 201, 21));
        label_3 = new QLabel(window_5_admin);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(60, 120, 141, 21));
        label_4 = new QLabel(window_5_admin);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(70, 220, 131, 21));
        label_5 = new QLabel(window_5_admin);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(30, 300, 171, 20));
        label_6 = new QLabel(window_5_admin);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(520, 300, 181, 20));
        label_7 = new QLabel(window_5_admin);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(520, 220, 131, 20));
        label_8 = new QLabel(window_5_admin);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(520, 120, 151, 20));
        Save = new QPushButton(window_5_admin);
        Save->setObjectName(QStringLiteral("Save"));
        Save->setGeometry(QRect(420, 370, 75, 23));
        Save->setStyleSheet(QLatin1String("background-color: rgb(255, 0, 0);\n"
"border-radius: 5px; \n"
"-moz-border-radius: 5px; \n"
"-webkit-border-radius: 5px; \n"
"\n"
"\n"
"\n"
" \n"
"text-shadow: #ff4346 0 0 2px; \n"
"border-bottom: 4px solid rgba(102,0,0,1); \n"
"color: #fff; \n"
"height: 20px;"));
        Repeat = new QPushButton(window_5_admin);
        Repeat->setObjectName(QStringLiteral("Repeat"));
        Repeat->setGeometry(QRect(230, 370, 75, 23));
        Repeat->setStyleSheet(QLatin1String("background-color: rgb(255, 0, 0);\n"
"border-radius: 5px; \n"
"-moz-border-radius: 5px; \n"
"-webkit-border-radius: 5px; \n"
"\n"
"\n"
"\n"
" \n"
"text-shadow: #ff4346 0 0 2px; \n"
"border-bottom: 4px solid rgba(102,0,0,1); \n"
"color: #fff; \n"
"height: 20px;"));
        exit = new QPushButton(window_5_admin);
        exit->setObjectName(QStringLiteral("exit"));
        exit->setGeometry(QRect(324, 370, 81, 23));
        exit->setStyleSheet(QLatin1String("background-color: rgb(255, 0, 0);\n"
"border-radius: 5px; \n"
"-moz-border-radius: 5px; \n"
"-webkit-border-radius: 5px; \n"
"\n"
"\n"
"\n"
" \n"
"text-shadow: #ff4346 0 0 2px; \n"
"border-bottom: 4px solid rgba(102,0,0,1); \n"
"color: #fff; \n"
"height: 20px;"));

        retranslateUi(window_5_admin);

        QMetaObject::connectSlotsByName(window_5_admin);
    } // setupUi

    void retranslateUi(QDialog *window_5_admin)
    {
        window_5_admin->setWindowTitle(QApplication::translate("window_5_admin", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("window_5_admin", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600;\">\320\222\320\270\320\264\321\213 \320\277\321\200\320\276\320\264\321\203\320\272\321\206\320\270\320\270 -----&gt;</span></p></body></html>", Q_NULLPTR));
        label_2->setText(QApplication::translate("window_5_admin", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600;\">&lt;----- \320\222\320\270\320\264\321\213 \320\277\321\200\320\276\320\264\321\203\320\272\321\206\320\270\320\270 </span></p></body></html>", Q_NULLPTR));
        label_3->setText(QApplication::translate("window_5_admin", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600;\">\320\241\320\261\320\276\321\200\321\211\320\270\320\272\320\270 -----&gt;</span></p></body></html>", Q_NULLPTR));
        label_4->setText(QApplication::translate("window_5_admin", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600;\">\320\221\321\200\320\270\320\263\320\260\320\264\321\213 -----&gt;</span></p></body></html>", Q_NULLPTR));
        label_5->setText(QApplication::translate("window_5_admin", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600;\">\320\226\321\203\321\200\320\275\320\260\320\273 \321\203\321\207\320\265\321\202\320\260 -----&gt;</span></p></body></html>", Q_NULLPTR));
        label_6->setText(QApplication::translate("window_5_admin", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600;\">&lt;----- \320\226\321\203\321\200\320\275\320\260\320\273 \321\203\321\207\320\265\321\202\320\260</span></p></body></html>", Q_NULLPTR));
        label_7->setText(QApplication::translate("window_5_admin", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600;\">&lt;----- \320\221\321\200\320\270\320\263\320\260\320\264\321\213</span></p></body></html>", Q_NULLPTR));
        label_8->setText(QApplication::translate("window_5_admin", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600;\">&lt;----- \320\241\320\261\320\276\321\200\321\211\320\270\320\272\320\270</span></p></body></html>", Q_NULLPTR));
        Save->setText(QApplication::translate("window_5_admin", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", Q_NULLPTR));
        Repeat->setText(QApplication::translate("window_5_admin", "\320\236\320\261\320\275\320\276\320\262\320\270\321\202\321\214", Q_NULLPTR));
        exit->setText(QApplication::translate("window_5_admin", "\320\222\321\213\321\205\320\276\320\264", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class window_5_admin: public Ui_window_5_admin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOW_5_ADMIN_H
