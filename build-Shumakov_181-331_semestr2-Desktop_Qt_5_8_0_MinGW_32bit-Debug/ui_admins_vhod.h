/********************************************************************************
** Form generated from reading UI file 'admins_vhod.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMINS_VHOD_H
#define UI_ADMINS_VHOD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_admins_vhod
{
public:
    QLineEdit *pole;
    QLineEdit *pole2;
    QLabel *label;
    QLabel *label_2;
    QPushButton *pushButton;

    void setupUi(QDialog *admins_vhod)
    {
        if (admins_vhod->objectName().isEmpty())
            admins_vhod->setObjectName(QStringLiteral("admins_vhod"));
        admins_vhod->resize(404, 309);
        admins_vhod->setStyleSheet(QStringLiteral("background-color: rgb(85, 170, 127);"));
        pole = new QLineEdit(admins_vhod);
        pole->setObjectName(QStringLiteral("pole"));
        pole->setGeometry(QRect(140, 60, 171, 41));
        pole->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        pole2 = new QLineEdit(admins_vhod);
        pole2->setObjectName(QStringLiteral("pole2"));
        pole2->setGeometry(QRect(140, 160, 171, 41));
        pole2->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        label = new QLabel(admins_vhod);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(50, 60, 71, 31));
        label_2 = new QLabel(admins_vhod);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(40, 160, 81, 31));
        pushButton = new QPushButton(admins_vhod);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(170, 250, 111, 31));
        pushButton->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"border-radius:5 px;"));

        retranslateUi(admins_vhod);

        QMetaObject::connectSlotsByName(admins_vhod);
    } // setupUi

    void retranslateUi(QDialog *admins_vhod)
    {
        admins_vhod->setWindowTitle(QApplication::translate("admins_vhod", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("admins_vhod", "<html><head/><body><p><span style=\" font-size:14pt; font-weight:600;\">\320\233\320\276\320\263\320\270\320\275</span></p></body></html>", Q_NULLPTR));
        label_2->setText(QApplication::translate("admins_vhod", "<html><head/><body><p><span style=\" font-size:14pt; font-weight:600;\">\320\237\320\260\321\200\320\276\320\273\321\214</span></p></body></html>", Q_NULLPTR));
        pushButton->setText(QApplication::translate("admins_vhod", "\320\222\320\276\320\271\321\202\320\270", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class admins_vhod: public Ui_admins_vhod {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMINS_VHOD_H
