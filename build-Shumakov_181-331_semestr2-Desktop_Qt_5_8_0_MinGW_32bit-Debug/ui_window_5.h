/********************************************************************************
** Form generated from reading UI file 'window_5.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOW_5_H
#define UI_WINDOW_5_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>

QT_BEGIN_NAMESPACE

class Ui_window_5
{
public:
    QPushButton *Product;
    QPushButton *Brigads;
    QPushButton *Rabi;
    QPushButton *Journal;
    QPushButton *Search;
    QPushButton *exit2;
    QLineEdit *search;
    QTextBrowser *textBrowser;
    QPushButton *member_password;
    QPushButton *member_login;

    void setupUi(QDialog *window_5)
    {
        if (window_5->objectName().isEmpty())
            window_5->setObjectName(QStringLiteral("window_5"));
        window_5->resize(771, 486);
        window_5->setStyleSheet(QStringLiteral("background-color: rgb(0, 0, 0);"));
        Product = new QPushButton(window_5);
        Product->setObjectName(QStringLiteral("Product"));
        Product->setGeometry(QRect(0, 0, 221, 61));
        Product->setStyleSheet(QLatin1String("background-color: rgb(255, 0, 0);\n"
"border-radius: 5px; \n"
"-moz-border-radius: 5px; \n"
"-webkit-border-radius: 5px; \n"
"\n"
"\n"
"\n"
" \n"
"text-shadow: #ff4346 0 0 2px; \n"
"border-bottom: 4px solid rgba(102,0,0,1); \n"
"color: #fff; \n"
"height: 20px;"));
        Brigads = new QPushButton(window_5);
        Brigads->setObjectName(QStringLiteral("Brigads"));
        Brigads->setGeometry(QRect(530, 0, 241, 61));
        Brigads->setStyleSheet(QLatin1String("background-color: rgb(255, 0, 0);\n"
"border-radius: 5px; \n"
"-moz-border-radius: 5px; \n"
"-webkit-border-radius: 5px; \n"
"\n"
"\n"
"\n"
" \n"
"text-shadow: #ff4346 0 0 2px; \n"
"border-bottom: 4px solid rgba(102,0,0,1); \n"
"color: #fff; \n"
"height: 20px;"));
        Rabi = new QPushButton(window_5);
        Rabi->setObjectName(QStringLiteral("Rabi"));
        Rabi->setGeometry(QRect(0, 420, 221, 61));
        Rabi->setStyleSheet(QLatin1String("background-color: rgb(255, 0, 0);\n"
"border-radius: 5px; \n"
"-moz-border-radius: 5px; \n"
"-webkit-border-radius: 5px; \n"
"\n"
"\n"
"\n"
" \n"
"text-shadow: #ff4346 0 0 2px; \n"
"border-bottom: 4px solid rgba(102,0,0,1); \n"
"color: #fff; \n"
"height: 20px;"));
        Journal = new QPushButton(window_5);
        Journal->setObjectName(QStringLiteral("Journal"));
        Journal->setGeometry(QRect(550, 420, 221, 61));
        Journal->setStyleSheet(QLatin1String("background-color: rgb(255, 0, 0);\n"
"border-radius: 5px; \n"
"-moz-border-radius: 5px; \n"
"-webkit-border-radius: 5px; \n"
"\n"
"\n"
"\n"
" \n"
"text-shadow: #ff4346 0 0 2px; \n"
"border-bottom: 4px solid rgba(102,0,0,1); \n"
"color: #fff; \n"
"height: 20px;"));
        Search = new QPushButton(window_5);
        Search->setObjectName(QStringLiteral("Search"));
        Search->setGeometry(QRect(330, 30, 91, 23));
        Search->setStyleSheet(QLatin1String("background-color: rgb(255, 0, 0);\n"
"border-radius: 5px; \n"
"-moz-border-radius: 5px; \n"
"-webkit-border-radius: 5px; \n"
"\n"
"\n"
"\n"
" \n"
"text-shadow: #ff4346 0 0 2px; \n"
"border-bottom: 4px solid rgba(102,0,0,1); \n"
"color: #fff; \n"
"height: 20px;"));
        exit2 = new QPushButton(window_5);
        exit2->setObjectName(QStringLiteral("exit2"));
        exit2->setGeometry(QRect(230, 430, 111, 41));
        exit2->setStyleSheet(QLatin1String("background-color: rgb(255, 0, 0);\n"
"border-radius: 5px; \n"
"-moz-border-radius: 5px; \n"
"-webkit-border-radius: 5px; \n"
"\n"
"\n"
"\n"
" \n"
"text-shadow: #ff4346 0 0 2px; \n"
"border-bottom: 4px solid rgba(102,0,0,1); \n"
"color: #fff; \n"
"height: 20px;"));
        search = new QLineEdit(window_5);
        search->setObjectName(QStringLiteral("search"));
        search->setGeometry(QRect(230, 10, 291, 20));
        search->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        textBrowser = new QTextBrowser(window_5);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(10, 80, 751, 331));
        textBrowser->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        member_password = new QPushButton(window_5);
        member_password->setObjectName(QStringLiteral("member_password"));
        member_password->setGeometry(QRect(350, 430, 91, 41));
        member_password->setStyleSheet(QLatin1String("background-color: rgb(255, 0, 0);\n"
"border-radius: 5px; \n"
"-moz-border-radius: 5px; \n"
"-webkit-border-radius: 5px; \n"
"\n"
"\n"
"\n"
" \n"
"text-shadow: #ff4346 0 0 2px; \n"
"border-bottom: 4px solid rgba(102,0,0,1); \n"
"color: #fff; \n"
"height: 20px;"));
        member_login = new QPushButton(window_5);
        member_login->setObjectName(QStringLiteral("member_login"));
        member_login->setGeometry(QRect(450, 430, 91, 41));
        member_login->setStyleSheet(QLatin1String("background-color: rgb(255, 0, 0);\n"
"border-radius: 5px; \n"
"-moz-border-radius: 5px; \n"
"-webkit-border-radius: 5px; \n"
"\n"
"\n"
"\n"
" \n"
"text-shadow: #ff4346 0 0 2px; \n"
"border-bottom: 4px solid rgba(102,0,0,1); \n"
"color: #fff; \n"
"height: 20px;"));

        retranslateUi(window_5);

        QMetaObject::connectSlotsByName(window_5);
    } // setupUi

    void retranslateUi(QDialog *window_5)
    {
        window_5->setWindowTitle(QApplication::translate("window_5", "Dialog", Q_NULLPTR));
        Product->setText(QApplication::translate("window_5", "\320\237\321\200\320\276\320\264\321\203\320\272\321\202\321\213", Q_NULLPTR));
        Brigads->setText(QApplication::translate("window_5", "\320\221\321\200\320\270\320\263\320\260\320\264\321\213", Q_NULLPTR));
        Rabi->setText(QApplication::translate("window_5", "\320\241\320\261\320\276\321\200\321\211\320\270\320\272\320\270", Q_NULLPTR));
        Journal->setText(QApplication::translate("window_5", "\320\226\321\203\321\200\320\275\320\260\320\273 \321\203\321\207\320\265\321\202\320\260", Q_NULLPTR));
        Search->setText(QApplication::translate("window_5", "\320\237\320\276\320\270\321\201\320\272", Q_NULLPTR));
        exit2->setText(QApplication::translate("window_5", "\320\222\321\213\321\205\320\276\320\264", Q_NULLPTR));
        member_password->setText(QApplication::translate("window_5", "\320\241\320\274\320\265\320\275\320\270\321\202\321\214  \320\277\320\260\321\200\320\276\320\273\321\214", Q_NULLPTR));
        member_login->setText(QApplication::translate("window_5", "\320\241\320\274\320\265\320\275\320\270\321\202\321\214 \320\273\320\276\320\263\320\270\320\275", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class window_5: public Ui_window_5 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOW_5_H
