/********************************************************************************
** Form generated from reading UI file 'window_6_member.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOW_6_MEMBER_H
#define UI_WINDOW_6_MEMBER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_window_6_member
{
public:
    QPushButton *member_password;
    QLabel *label;
    QLineEdit *old_password;
    QLineEdit *new_password;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *old_login_3;
    QLabel *label_4;

    void setupUi(QDialog *window_6_member)
    {
        if (window_6_member->objectName().isEmpty())
            window_6_member->setObjectName(QStringLiteral("window_6_member"));
        window_6_member->resize(726, 470);
        window_6_member->setStyleSheet(QStringLiteral("background-color: rgb(170, 85, 127);"));
        member_password = new QPushButton(window_6_member);
        member_password->setObjectName(QStringLiteral("member_password"));
        member_password->setGeometry(QRect(300, 420, 151, 41));
        member_password->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        label = new QLabel(window_6_member);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(250, 10, 221, 61));
        old_password = new QLineEdit(window_6_member);
        old_password->setObjectName(QStringLiteral("old_password"));
        old_password->setGeometry(QRect(370, 230, 171, 31));
        old_password->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        new_password = new QLineEdit(window_6_member);
        new_password->setObjectName(QStringLiteral("new_password"));
        new_password->setGeometry(QRect(370, 310, 171, 31));
        new_password->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        label_2 = new QLabel(window_6_member);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(180, 230, 181, 31));
        label_3 = new QLabel(window_6_member);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(190, 310, 171, 31));
        old_login_3 = new QLineEdit(window_6_member);
        old_login_3->setObjectName(QStringLiteral("old_login_3"));
        old_login_3->setGeometry(QRect(370, 150, 171, 31));
        old_login_3->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        label_4 = new QLabel(window_6_member);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(250, 150, 111, 31));

        retranslateUi(window_6_member);

        QMetaObject::connectSlotsByName(window_6_member);
    } // setupUi

    void retranslateUi(QDialog *window_6_member)
    {
        window_6_member->setWindowTitle(QApplication::translate("window_6_member", "Dialog", Q_NULLPTR));
        member_password->setText(QApplication::translate("window_6_member", "\320\241\320\274\320\265\320\275\320\270\321\202\321\214 \320\277\320\260\321\200\320\276\320\273\321\214", Q_NULLPTR));
        label->setText(QApplication::translate("window_6_member", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-style:italic;\">\320\250\320\276, \320\276\320\277\321\217\321\202\321\214 \320\267\320\260\320\261\321\213\320\273?</span></p></body></html>", Q_NULLPTR));
        label_2->setText(QApplication::translate("window_6_member", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-style:italic;\">\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \321\201\321\202\320\260\321\200\321\213\320\271 \320\277\320\260\321\200\320\276\320\273\321\214</span></p></body></html>", Q_NULLPTR));
        label_3->setText(QApplication::translate("window_6_member", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-style:italic;\">\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\275\320\276\320\262\321\213\320\271 \320\277\320\260\321\200\320\276\320\273\321\214</span></p></body></html>", Q_NULLPTR));
        label_4->setText(QApplication::translate("window_6_member", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-style:italic;\">\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\273\320\276\320\263\320\270\320\275</span></p></body></html>", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class window_6_member: public Ui_window_6_member {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOW_6_MEMBER_H
