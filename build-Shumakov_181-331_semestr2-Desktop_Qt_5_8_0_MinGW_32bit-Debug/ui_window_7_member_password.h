/********************************************************************************
** Form generated from reading UI file 'window_7_member_password.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOW_7_MEMBER_PASSWORD_H
#define UI_WINDOW_7_MEMBER_PASSWORD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_window_7_member_password
{
public:
    QLabel *label;
    QLineEdit *old_password;
    QLineEdit *new_password;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *old_login;
    QLabel *label_4;
    QPushButton *save_password;

    void setupUi(QDialog *window_7_member_password)
    {
        if (window_7_member_password->objectName().isEmpty())
            window_7_member_password->setObjectName(QStringLiteral("window_7_member_password"));
        window_7_member_password->resize(712, 474);
        window_7_member_password->setStyleSheet(QStringLiteral("background-color: rgb(255, 151, 153);"));
        label = new QLabel(window_7_member_password);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(240, 10, 221, 41));
        old_password = new QLineEdit(window_7_member_password);
        old_password->setObjectName(QStringLiteral("old_password"));
        old_password->setGeometry(QRect(350, 220, 171, 31));
        old_password->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        new_password = new QLineEdit(window_7_member_password);
        new_password->setObjectName(QStringLiteral("new_password"));
        new_password->setGeometry(QRect(350, 290, 171, 31));
        new_password->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        label_2 = new QLabel(window_7_member_password);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(170, 290, 171, 31));
        label_3 = new QLabel(window_7_member_password);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(160, 220, 181, 31));
        old_login = new QLineEdit(window_7_member_password);
        old_login->setObjectName(QStringLiteral("old_login"));
        old_login->setGeometry(QRect(350, 150, 171, 31));
        old_login->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        label_4 = new QLabel(window_7_member_password);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(220, 150, 121, 31));
        save_password = new QPushButton(window_7_member_password);
        save_password->setObjectName(QStringLiteral("save_password"));
        save_password->setGeometry(QRect(300, 420, 111, 23));
        save_password->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        retranslateUi(window_7_member_password);

        QMetaObject::connectSlotsByName(window_7_member_password);
    } // setupUi

    void retranslateUi(QDialog *window_7_member_password)
    {
        window_7_member_password->setWindowTitle(QApplication::translate("window_7_member_password", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("window_7_member_password", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-style:italic;\">\320\241\320\274\320\265\320\275\320\260 \320\277\320\260\321\200\320\276\320\273\321\217</span></p></body></html>", Q_NULLPTR));
        label_2->setText(QApplication::translate("window_7_member_password", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-style:italic;\">\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\275\320\276\320\262\321\213\320\271 \320\277\320\260\321\200\320\276\320\273\321\214</span></p></body></html>", Q_NULLPTR));
        label_3->setText(QApplication::translate("window_7_member_password", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-style:italic;\">\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \321\201\321\202\320\260\321\200\321\213\320\271 \320\277\320\260\321\200\320\276\320\273\321\214</span></p></body></html>", Q_NULLPTR));
        label_4->setText(QApplication::translate("window_7_member_password", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-style:italic;\">\320\222\320\262\320\265\320\264\320\270\321\202\320\265  \320\273\320\276\320\263\320\270\320\275</span></p></body></html>", Q_NULLPTR));
        save_password->setText(QApplication::translate("window_7_member_password", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class window_7_member_password: public Ui_window_7_member_password {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOW_7_MEMBER_PASSWORD_H
