#ifndef WINDOW_7_MEMBER_PASSWORD_H
#define WINDOW_7_MEMBER_PASSWORD_H

#include <QDialog>

namespace Ui {
class window_7_member_password;
}

class window_7_member_password : public QDialog
{
    Q_OBJECT

public:
    explicit window_7_member_password(QWidget *parent = nullptr);
    ~window_7_member_password();

private slots:
    void on_save_password_clicked();

private:
    Ui::window_7_member_password *ui;
};

#endif // WINDOW_7_MEMBER_PASSWORD_H
