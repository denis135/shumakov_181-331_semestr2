#include "window_4.h"
#include "ui_window_4.h"
#include "window_3.h"

window_4::window_4(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::window_4)
{
    ui->setupUi(this);
}

window_4::~window_4()
{
    delete ui;
}

void window_4::on_vhod2_clicked()
{
    window_3 window;
    hide();
    window.setModal(true);
    window.exec();
}
