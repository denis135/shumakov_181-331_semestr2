#ifndef WINDOW_3_H
#define WINDOW_3_H

#include <QDialog>
#include <QFile>
#include <QTextBrowser>

namespace Ui {
class window_3;
}

class window_3 : public QDialog
{
    Q_OBJECT

public:
    explicit window_3(QWidget *parent = nullptr);
    ~window_3();

private slots:
    void on_vhod_clicked();

    void on_pushButton_clicked();

    void on_admins_vhod_clicked();

    void on_member_clicked();

    void on_member1_clicked();

private:
    Ui::window_3 *ui;
    QFile file;

};

#endif // WINDOW_3_H
