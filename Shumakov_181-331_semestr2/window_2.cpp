#include "window_2.h"
#include "ui_window_2.h"
#include "window_4.h"
#include "QMessageBox"
#include <QMainWindow>
#include <QFile>
#include <fstream>
#include <QTextStream>


window_2::window_2(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::window_2)
{
    ui->setupUi(this);
}

window_2::~window_2()
{
    delete ui;
}



void window_2::on_Reg_clicked()
{

    QString pole= ui->pole->text();
    QString pole2 = ui->pole2->text();

    if(pole == "" && pole2 == "") {
        QMessageBox::warning(this, "Error", "Заполните пустые поля!!!");
      }
    else{

        QFile file("C:\\baza.txt");
        if(file.open(QIODevice::Append))
        {
          QTextStream outRegistr(&file);
          outRegistr << pole <<";";
          outRegistr << pole2 <<";\r\n";
          file.close();
        }


        window_4 window;
        hide();
        window.setModal(true);
        window.exec();
    }
}

