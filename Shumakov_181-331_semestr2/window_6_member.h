#ifndef WINDOW_6_MEMBER_H
#define WINDOW_6_MEMBER_H

#include <QDialog>

namespace Ui {
class window_6_member;
}

class window_6_member : public QDialog
{
    Q_OBJECT

public:
    explicit window_6_member(QWidget *parent = nullptr);
    ~window_6_member();

private slots:
    void on_member_password_clicked();

    void on_member_login_clicked();

private:
    Ui::window_6_member *ui;
};

#endif // WINDOW_6_MEMBER_H
