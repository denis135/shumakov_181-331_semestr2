#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"
#include "window_2.h"
#include <QFile>
#include "window_3.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_Autorizaton_clicked()
{
    window_3 window;
    hide();
    window.setModal(true);
    window.exec();
}

void MainWindow::on_Registr_clicked()
{
    window_2 window;
    hide();
    window.setModal(true);
    window.exec();
}
