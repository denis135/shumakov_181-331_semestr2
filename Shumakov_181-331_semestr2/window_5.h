#ifndef WINDOW_5_H
#define WINDOW_5_H

#include <QDialog>

namespace Ui {
class window_5;
}

class window_5 : public QDialog
{
    Q_OBJECT

public:
    explicit window_5(QWidget *parent = nullptr);
    ~window_5();

private slots:
    void on_pushButton_clicked();

    void on_Product_clicked();

    void on_Brigads_clicked();

    void on_Rabi_clicked();

    void on_Journal_clicked();

    void on_pushButton_5_clicked();

    void on_exit2_clicked();

    void on_Search_clicked();

    void on_member_clicked();

    void on_member_password_clicked();

    void on_member_login_clicked();

private:
    Ui::window_5 *ui;
};

#endif // WINDOW_5_H
