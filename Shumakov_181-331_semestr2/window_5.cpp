#include "window_5.h"
#include "ui_window_5.h"
#include <QFile>
#include <QMainWindow>
#include <fstream>
#include <QTextStream>
#include <QString>
#include "window_3.h"
#include "window_7_member_password.h"
#include "window_8_member_login.h"


window_5::window_5(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::window_5)
{
    ui->setupUi(this);
}

window_5::~window_5()
{
    delete ui;
}


void window_5::on_Product_clicked()
{

    QFile file("C:\\products.txt");
    if ((file.exists())&&(file.open(QIODevice::ReadOnly)))
    {
        QString str="";
        while(!file.atEnd())
        {
            str=str+file.readLine();
        }
        ui->textBrowser->clear();
        ui->textBrowser->setText(str);
        file.close();
    }
}

void window_5::on_Brigads_clicked()
{
    QFile file2("C:\\brigads.txt");
    if ((file2.exists())&&(file2.open(QIODevice::ReadOnly)))
    {
        QString str="";
        while(!file2.atEnd())
        {
            str=str+file2.readLine();
        }
        ui->textBrowser->clear();
        ui->textBrowser->setText(str);
        file2.close();
    }
}

void window_5::on_Rabi_clicked()
{
    QFile file1("C:\\rabi.txt");

    if ((file1.exists())&&(file1.open(QIODevice::ReadOnly)))
    {
        QString str="";
        while(!file1.atEnd())
        {
            str=str+file1.readLine();
        }
        ui->textBrowser->clear();
        ui->textBrowser->setText(str);
        file1.close();
    }
}

void window_5::on_Journal_clicked()
{
    QFile file3("C:\\journal.txt");

    if ((file3.exists())&&(file3.open(QIODevice::ReadOnly)))
    {
        QString str="";
        while(!file3.atEnd())
        {
            str=str+file3.readLine();
        }
        ui->textBrowser->clear();
        ui->textBrowser->setText(str);
        file3.close();
    }
}
void window_5::on_exit2_clicked()
{
    window_3 window;
    hide();
    window.setModal(true);
    window.exec();

}

void window_5::on_Search_clicked()
{
     QString search = ui ->search->text();
     std::string search1 = search.toUtf8().constData();
     std::ifstream inpute("C:\\search.txt", std::fstream::in);    
     std::string line;
     QFile file4("search.txt");
     bool flag=false;
     while (getline(inpute, line))
     {

              if (line == (search1))
              {

                      QString str;
                      str=str+search;
                      ui->textBrowser->setText(str);
                      flag=true;
                      break;
               }

       }
       if(flag==false){
            QString str1="По вашему запросу ничего не найдено";
            ui->textBrowser->setText(str1);
     }

 inpute.close();

}



void window_5::on_member_password_clicked()
{
    window_7_member_password window;
    hide();
    window.setModal(true);
    window.exec();
}

void window_5::on_member_login_clicked()
{
    window_8_member_login window;
    hide();
    window.setModal(true);
    window.exec();
}
