#ifndef WINDOW_8_MEMBER_LOGIN_H
#define WINDOW_8_MEMBER_LOGIN_H

#include <QDialog>

namespace Ui {
class window_8_member_login;
}

class window_8_member_login : public QDialog
{
    Q_OBJECT

public:
    explicit window_8_member_login(QWidget *parent = nullptr);
    ~window_8_member_login();

private slots:
    void on_save_login_clicked();

private:
    Ui::window_8_member_login *ui;
};

#endif // WINDOW_8_MEMBER_LOGIN_H
