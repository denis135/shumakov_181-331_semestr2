#ifndef WINDOW_5_ADMIN_H
#define WINDOW_5_ADMIN_H

#include <QDialog>

namespace Ui {
class window_5_admin;
}

class window_5_admin : public QDialog
{
    Q_OBJECT

public:
    explicit window_5_admin(QWidget *parent = nullptr);
    ~window_5_admin();

private slots:
    void on_Save_clicked();

    void on_Repeat_clicked();

    void on_exit_clicked();

private:
    Ui::window_5_admin *ui;
};

#endif // WINDOW_5_ADMIN_H
