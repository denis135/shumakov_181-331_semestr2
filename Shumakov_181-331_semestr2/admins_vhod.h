#ifndef ADMINS_VHOD_H
#define ADMINS_VHOD_H

#include <QDialog>

namespace Ui {
class admins_vhod;
}

class admins_vhod : public QDialog
{
    Q_OBJECT

public:
    explicit admins_vhod(QWidget *parent = nullptr);
    ~admins_vhod();

private slots:
    void on_pushButton_clicked();

private:
    Ui::admins_vhod *ui;
};

#endif // ADMINS_VHOD_H
