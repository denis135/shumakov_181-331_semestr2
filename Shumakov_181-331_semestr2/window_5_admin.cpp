#include "window_5_admin.h"
#include "ui_window_5_admin.h"
#include <QFile>
#include <QMainWindow>
#include <fstream>
#include <QTextStream>
#include <QString>
#include <window_3.h>



window_5_admin::window_5_admin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::window_5_admin)
{
    ui->setupUi(this);
}

window_5_admin::~window_5_admin()
{
    delete ui;
}

void window_5_admin::on_Save_clicked()
{
    QString products1 = ui->products->toPlainText();
    QString rabi1 = ui->rabi->toPlainText();
    QString brigads1 = ui ->brigads->toPlainText();
    QString journal1= ui->journal->toPlainText();
    QFile file("C:\\products.txt");
    QFile file1("C:\\rabi.txt");
    QFile file2("C:\\brigads.txt");
    QFile file3("C:\\journal.txt");
    QFile file4("C:\\search.txt");
    if(file.open(QIODevice::WriteOnly))
    {
      QTextStream Products(&file);
      Products << products1 <<"\r\n";
      file.close();
    }
    if(file1.open(QIODevice::WriteOnly))
     {
      QTextStream Rabi(&file1);
      Rabi << rabi1 <<"\r\n";
      file1.close();
     }

    if(file2.open(QIODevice::WriteOnly))
     {
      QTextStream Brigads(&file2);
      Brigads << brigads1 <<"\r\n";
      file2.close();

     }
    if(file3.open(QIODevice::WriteOnly))
     {
      QTextStream Journey(&file3);
      Journey << journal1 <<"\r\n";
      file3.close();
     }



    //Поиск
    if(file4.open(QIODevice::Append))
    {
      QTextStream Products(&file4);
      QTextStream Rabi(&file4);
      QTextStream Brigads(&file4);
      QTextStream Journal(&file4);
      Products << products1 <<"\r\n";
      Rabi << rabi1 <<"\r\n";
      Brigads << brigads1 <<"\r\n";
      Journal << journal1 <<"\r\n";
      file4.close();
    }

}

void window_5_admin::on_Repeat_clicked()
{

    QFile file("C:\\products.txt");
    QFile file1("C:\\rabi.txt");
    QFile file2("C:\\brigads.txt");
    QFile file3("C:\\journal.txt");
    if ((file.exists())&&(file.open(QIODevice::ReadOnly)))
    {
        QString str="";
        while(!file.atEnd())
        {
            str=str+file.readLine();
        }
        ui->products->setText(str);
        file.close();
    }

    if ((file1.exists())&&(file1.open(QIODevice::ReadOnly)))
    {
        QString str="";
        while(!file1.atEnd())
        {
            str=str+file1.readLine();
        }
        ui->rabi->setText(str);
        file1.close();
    }

    if ((file2.exists())&&(file2.open(QIODevice::ReadOnly)))
    {
        QString str="";
        while(!file2.atEnd())
        {
           str=str+file2.readLine();
        }
        ui->brigads->setText(str);
        file2.close();
    }

    if ((file3.exists())&&(file3.open(QIODevice::ReadOnly)))
    {
        QString str="";
        while(!file3.atEnd())
        {
            str=str+file3.readLine();
        }
        ui->journal->setText(str);
        file3.close();
    }
}

void window_5_admin::on_exit_clicked()
{
    window_3 window;
    hide();
    window.setModal(true);
    window.exec();

}
