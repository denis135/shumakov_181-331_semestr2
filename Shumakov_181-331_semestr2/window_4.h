#ifndef WINDOW_4_H
#define WINDOW_4_H

#include <QDialog>

namespace Ui {
class window_4;
}

class window_4 : public QDialog
{
    Q_OBJECT

public:
    explicit window_4(QWidget *parent = nullptr);
    ~window_4();

private slots:
    void on_vhod2_clicked();

private:
    Ui::window_4 *ui;
};

#endif // WINDOW_4_H
