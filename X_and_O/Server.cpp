#include "server.h"

#include <QTcpServer>
#include <QTcpSocket>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QLabel>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QLabel>

#include <QDataStream>
#include <QTime>

Server::Server(quint16 port): _nextBlockSize(0){
    _tcpServer = new QTcpServer(this);

    if (!_tcpServer->listen(QHostAddress::Any, port)){
        _tcpServer->close();
        return;
    }

    connect(_tcpServer, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));

   _text = new QTextEdit();
   _text->setReadOnly(true);
    setWindowTitle ( "server" );
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(new QLabel("<H1>Server</H1>"));
    layout->addWidget(_text);
    setLayout(layout);
    resize(200,200);
}

void Server::slotNewConnection(){
    sockets.append(_tcpServer->nextPendingConnection());
    connect(sockets.last(),SIGNAL(readyRead()),this, SLOT(slotReadClient()));
    if (sockets.size() == 1) {
        playon = 1;
        sendToClient(sockets.last(), "you gaming is ""X");
    }
    else if (sockets.size() == 2) {
        playon = 2;
        sendToClient(sockets.last(), "you gaming is ""O, wait first player");
        sendToClient(sockets.at(0), "you FIRST");
    }
}

void Server::slotReadClient(){
    for (int i = 0; i < sockets.size(); i++)
    {
        QDataStream in(sockets.at(i));
        while(true)
        {
            if (_nextBlockSize == 0)
            {
                if (sockets.at(i)->bytesAvailable() <static_cast<int>(sizeof(quint16)))
                {
                    break;
                }
                in >> _nextBlockSize;
            }

            if (sockets.at(i)->bytesAvailable() < _nextBlockSize)
            {
                break;
            }

            QTime time;
            QString str;
            in >> time >> str;

            if (playon == 2)
            {
                if (i == 0)
                {
                 int i_k = str.split(' ')[0].toInt() % 3;
                 int j_k = str.split(' ')[1].toInt() % 3;
                 if (play[i_k][j_k] != "|")
                 {
                        QString message = time.toString() + " " + "Client has sent - " + str + ".";
                        _text->append(message);
                        _nextBlockSize = 0;
                        sendToClient(sockets.at(0), "please, choose another cell");
                 }
                 else {
                        play[i_k][j_k] = "X";
                        if (wins())
                        {
                            play = {{" ", " ", " "}, {" ", " ", " "}, {" ", " ", " "}};
                            _nextBlockSize = 0;
                            sendToClient(sockets.at(0), "YOU WIN!");
                            _nextBlockSize = 0;
                            sendToClient(sockets.last(), "YOU LOSE(((");
                        }
                        else if (ends())
                        {
                            play = {{" ", " ", " "}, {" ", " ", " "}, {" ", " ", " "}};
                            _nextBlockSize = 0;
                            sendToClient(sockets.at(0), "DRAW");
                            _nextBlockSize = 0;
                            sendToClient(sockets.last(), "DRAW");
                        }
                        else
                        {
                            playon = 3;
                            _nextBlockSize = 0;
                            sendToClient(sockets.at(0), "Wait second player");
                            print(sockets.at(0));

                            _nextBlockSize = 0;
                            sendToClient(sockets.last(), "Go on");
                            print(sockets.last());
                        }
                    }
                }
                else if (i == 1)
                {
                    QString message = time.toString() + " " + "Client has sent - " + str + ".";
                    _text->append(message);
                    _nextBlockSize = 0;
                    sendToClient(sockets.last(), "Wait");
                }
            }
            else if (playon == 3)
            {
                if (str != "")
                {
                    if (i == 1)
                    {
                        int i_k = str.split(' ')[0].toInt();
                        int j_k = str.split(' ')[1].toInt();
                        if (play[i_k][j_k] != "|") {
                            QString message = time.toString() + " " + "Client has sent - " + str + ".";
                            _text->append(message);
                            _nextBlockSize = 0;
                            sendToClient(sockets.last(), "please, choose another cell");
                        }
                        else {
                            play[i_k][j_k] = "O";
                            if (wins())
                            {
                                playon = 2;
                                play = {{" ", " ", " "}, {" ", " ", " "}, {" ", " ", " "}};
                                _nextBlockSize = 0;
                                sendToClient(sockets.last(), "YOU WIN!");
                                _nextBlockSize = 0;
                                sendToClient(sockets.at(0), "YOU LOSE(((");
                            }
                            else if (ends())
                            {
                                playon = 2;
                                play = {{" ", " ", " "}, {" ", " ", " "}, {" ", " ", " "}};
                                _nextBlockSize = 0;
                                sendToClient(sockets.at(0), "DRAW");
                                _nextBlockSize = 0;
                                sendToClient(sockets.last(), "DRAW");
                            }
                            else
                            {
                                playon = 2;


                                _nextBlockSize = 0;
                                sendToClient(sockets.last(), "Wait first player");
                                print(sockets.last());
                                _nextBlockSize = 0;
                                sendToClient(sockets.at(0), "Wait second player");
                                print(sockets.at(0));
                            }
                        }
                    }
                    else if (i == 0)
                    {
                        QString message = time.toString() + " " + "Client has sent - " + str + ".";
                        _text->append(message);
                        _nextBlockSize = 0;
                        sendToClient(sockets.at(0), "Wait");
                    }
                }

            }
            else if (playon == 1)
            {
                QString message = time.toString() + " " + "Client has sent - " + str + ".";
                _text->append(message);
                _nextBlockSize = 0;
                sendToClient(sockets.at(0), "We waiting for second player.");
            }
        }

    }
}

bool Server::wins()
{
    for (unsigned int i = 0; i < 3; i++)
    {
        if (play[i][0] == play[i][1] && play[i][0] == play[i][2] && play[i][0] != "|") return true;
    }
    for (unsigned int i = 0; i < 3; i++)
    {
        if (play[0][i] == play[1][i] && play[0][i] == play[2][i] && play[0][i] != "|") return true;
    }
    if (play[0][0] == play[1][1] && play[1][1] == play[2][2] && play[0][0] != "|") return true;
    if (play[0][2] == play[1][1] && play[1][1] == play[2][0] && play[1][1] != "|") return true;
    return false;
}

bool Server::ends()
{
    if (!wins())
    {
        for (unsigned int i = 0; i < 3; i++){
            for (unsigned int j = 0; j < 3; j++){
                if (play[i][j] == "|") return false;
            }
        }
    }
    else return true;
    return true;
}

void Server::print(QTcpSocket * socket)
{
    for (unsigned int i = 0; i < 3; i++)
    {
        QString line;
        line ="[" + play[i][0] + "]"+"[" + play[i][1] + "]" + "[" + play[i][2] + "]";
        _nextBlockSize = 0;
        sendToClient(socket, line);
    }
}

void Server::sendToClient(QTcpSocket* socket, const QString &str){


    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);


    out << quint16(0) << QTime::currentTime() << str;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

    socket->write(arrBlock);

}
