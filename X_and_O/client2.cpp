#include "client.h"
#include "client2.h"
#include <QTcpSocket>

#include <QTextEdit>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QLabel>
#include <QTime>

Client2::Client2(const QString &strHost, quint16 port): _nextBlockSize2(0){
    _tcpSocket2 = new QTcpSocket(this);
    _tcpSocket2->connectToHost(strHost, port);

    connect(_tcpSocket2, SIGNAL(connected()), this, SLOT(slotConnected()));
    connect(_tcpSocket2, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));

    _textInfo2 = new QTextEdit();
    _textInput2 = new QLineEdit();
    _textInfo2->setReadOnly(true);
    QPushButton *button = new QPushButton("Send");
    connect(button, SIGNAL(clicked()), SLOT(slotSendToServer()));
    connect(_textInput2, SIGNAL(returnPressed()), this, SLOT(slotSendToServer()));

    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(new QLabel("<H1>Player_2</H1>"));
    layout->addWidget(_textInfo2);
    layout->addWidget(_textInput2);
    layout->addWidget(button);
    setLayout(layout);
}

void Client2::slotReadyRead(){
    QDataStream in(_tcpSocket2);
    //in.setVersion(QDataStream::Qt_5_10);

    while(true)
    {
        if (_nextBlockSize2 == 0){
            if (_tcpSocket2->bytesAvailable() <static_cast<int>(sizeof(quint16))){
                break;
            }

            in >> _nextBlockSize2;
        }

        if (_tcpSocket2->bytesAvailable() < _nextBlockSize2){
            break;
        }

        QTime time;
        QString str;
        in >> time >> str;
        _textInfo2->append(time.toString() + " " + str);

        _nextBlockSize2 = 0;
    }
}

void Client2::slotSendToServer(){
    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    //out.setVersion(QDataStream::Qt_5_10);

    out << quint16(0) << QTime::currentTime() << _textInput2->text();
    out.device()->seek(0);
    out<< quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

    _tcpSocket2->write(arrBlock);
    _textInput2->setText("");
}

void Client2::slotConnected(){
    _textInfo2->append("Received the \"connected\" signal.");
}

/*void Client::slotError(QAbstractSocket::SocketError err)

}*/



