/********************************************************************************
** Form generated from reading UI file 'window_5.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOW_5_H
#define UI_WINDOW_5_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_window_5
{
public:

    void setupUi(QDialog *window_5)
    {
        if (window_5->objectName().isEmpty())
            window_5->setObjectName(QStringLiteral("window_5"));
        window_5->resize(677, 300);

        retranslateUi(window_5);

        QMetaObject::connectSlotsByName(window_5);
    } // setupUi

    void retranslateUi(QDialog *window_5)
    {
        window_5->setWindowTitle(QApplication::translate("window_5", "Dialog", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class window_5: public Ui_window_5 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOW_5_H
