/********************************************************************************
** Form generated from reading UI file 'window_3.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOW_3_H
#define UI_WINDOW_3_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_window_3
{
public:
    QLabel *label;
    QLabel *label_2;
    QLineEdit *pole;
    QLabel *label_3;
    QLineEdit *pole2;
    QPushButton *vhod;

    void setupUi(QDialog *window_3)
    {
        if (window_3->objectName().isEmpty())
            window_3->setObjectName(QStringLiteral("window_3"));
        window_3->resize(496, 423);
        window_3->setStyleSheet(QLatin1String("background-color: rgb(150, 255, 148);\n"
"background-color: rgb(255, 170, 170);"));
        label = new QLabel(window_3);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(140, 20, 201, 31));
        label_2 = new QLabel(window_3);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(170, 80, 131, 21));
        pole = new QLineEdit(window_3);
        pole->setObjectName(QStringLiteral("pole"));
        pole->setGeometry(QRect(142, 119, 181, 31));
        pole->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        label_3 = new QLabel(window_3);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(160, 180, 151, 21));
        pole2 = new QLineEdit(window_3);
        pole2->setObjectName(QStringLiteral("pole2"));
        pole2->setGeometry(QRect(142, 229, 181, 31));
        pole2->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        vhod = new QPushButton(window_3);
        vhod->setObjectName(QStringLiteral("vhod"));
        vhod->setGeometry(QRect(160, 322, 151, 31));
        vhod->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        retranslateUi(window_3);

        QMetaObject::connectSlotsByName(window_3);
    } // setupUi

    void retranslateUi(QDialog *window_3)
    {
        window_3->setWindowTitle(QApplication::translate("window_3", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("window_3", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600; color:#00007f;\">\320\220\320\262\321\202\320\276\321\200\320\270\320\267\320\260\321\206\320\270\321\217</span></p></body></html>", Q_NULLPTR));
        label_2->setText(QApplication::translate("window_3", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-weight:600;\">\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\273\320\276\320\263\320\270\320\275</span></p></body></html>", Q_NULLPTR));
        label_3->setText(QApplication::translate("window_3", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt; font-weight:600;\">\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\277\320\260\321\200\320\276\320\273\321\214</span></p></body></html>", Q_NULLPTR));
        vhod->setText(QApplication::translate("window_3", "\320\222\320\276\320\271\321\202\320\270", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class window_3: public Ui_window_3 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOW_3_H
