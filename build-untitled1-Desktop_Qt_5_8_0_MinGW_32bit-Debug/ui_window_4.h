/********************************************************************************
** Form generated from reading UI file 'window_4.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOW_4_H
#define UI_WINDOW_4_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_window_4
{
public:
    QLabel *label;
    QPushButton *vhod2;

    void setupUi(QDialog *window_4)
    {
        if (window_4->objectName().isEmpty())
            window_4->setObjectName(QStringLiteral("window_4"));
        window_4->resize(504, 300);
        window_4->setStyleSheet(QStringLiteral("background-color: rgb(170, 85, 255);"));
        label = new QLabel(window_4);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, -10, 511, 211));
        vhod2 = new QPushButton(window_4);
        vhod2->setObjectName(QStringLiteral("vhod2"));
        vhod2->setGeometry(QRect(120, 182, 261, 61));
        vhod2->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        retranslateUi(window_4);

        QMetaObject::connectSlotsByName(window_4);
    } // setupUi

    void retranslateUi(QDialog *window_4)
    {
        window_4->setWindowTitle(QApplication::translate("window_4", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("window_4", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600; font-style:italic; color:#000000;\">\320\222\321\213 \321\203\321\201\320\277\320\265\321\210\320\275\320\276 \320\267\320\260\321\200\320\265\320\263\320\270\321\201\321\202\321\200\320\270\321\200\320\276\320\262\320\260\320\275\321\213!!!</span></p></body></html>", Q_NULLPTR));
        vhod2->setText(QApplication::translate("window_4", "\320\222\320\276\320\271\321\202\320\270", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class window_4: public Ui_window_4 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOW_4_H
